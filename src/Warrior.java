public class Warrior {
    private int baseHP;
    private int wp;

    public Warrior(int baseHP, int wp) {
        if (baseHP < 1 || baseHP > 888) {
            throw new RuntimeException("baseHP of Warrior must be in the range [1..888]");
        }
        if (wp < 0 || wp > 1) {
            throw new RuntimeException("wp of Warrior must be in the range[0..1]");
        }
        this.baseHP = baseHP;
        this.wp = wp;
    }

    public int getBaseHP() {
        return this.baseHP;
    }

    public int getwp() {
        return this.wp;
    }

    public int getRealHP() {
        if (this.wp == 1) {
            return this.baseHP;
        }
        else {
            return this.baseHP / 10;
        }
    }
}
