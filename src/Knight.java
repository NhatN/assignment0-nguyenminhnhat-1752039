public class Knight {
    private int baseHP;
    private int wp;

    public Knight(int baseHP, int wp) {
        if (baseHP < 99 || baseHP > 999) {
            throw new RuntimeException("baseHP of Knight must be in the range [99..999]");
        }
        if (wp < 0 || wp > 1) {
            throw new RuntimeException("wp of Knight must be in the range [0..1]");
        }
        this.baseHP = baseHP;
        this.wp = wp;
    }

    public int getBaseHP() {
        return this.baseHP;
    }

    public int getwp() {
        return this.wp;
    }

    public int getRealHP() {
        if (this.wp == 1) {
            return this.baseHP;
        }
        else {
            return this.baseHP / 10;
        }
    }
}
