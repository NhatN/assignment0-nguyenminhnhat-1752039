import java.util.Scanner;

public class Launcher {
    public static void main (String[] args)
    {
        double p;
        Knight knight;
        Warrior warrior;
        int HP, wp;

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter base HP (99-999) and weapon type (0-1) of knight: ");
        HP = sc.nextInt();
        while (HP<99 || HP>999)
        {
            System.out.println("Error ... BaseHP must be 99-999");
            HP = sc.nextInt();
        }
        wp = sc.nextInt();
        while (wp<0 || wp>1)
        {
            System.out.println("Error ... Weapon must be type 0-1");
            wp = sc.nextInt();
        }
        knight = new Knight(HP, wp);
        System.out.print("Enter base HP (1-888) and weapon type (0-1) of warrior: ");
        HP = sc.nextInt();
        while (HP<1 || HP>888)
        {
            System.out.println("Error ... BaseHP must be 1-888");
            HP = sc.nextInt();
        }
        wp = sc.nextInt();
        while (wp<0 || wp>1)
        {
            System.out.println("Error ... Weapon must be type 0-1");
            wp = sc.nextInt();
        }
        warrior = new Warrior(HP, wp);

        System.out.println();
        p = (double)((knight.getRealHP() - warrior.getRealHP()) + 999) / 2000;
        System.out.println("The odd of the knight victory is: " + p);
        sc.close();
    }
}
